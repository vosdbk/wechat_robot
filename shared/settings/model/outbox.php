<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['models']['outbox']=array (
  'id' => '2',
  'name' => 'outbox',
  'description' => '发件箱',
  'perpage' => '20',
  'hasattach' => '0',
  'built_in' => '0',
  'fields' => 
  array (
    3 => 
    array (
      'id' => '3',
      'name' => 'to_user',
      'description' => '收信人',
      'model' => '2',
      'type' => 'input',
      'length' => '50',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '1',
      'editable' => '1',
    ),
    4 => 
    array (
      'id' => '4',
      'name' => 'xml_data',
      'description' => '信息数据',
      'model' => '2',
      'type' => 'textarea',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '200',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '2',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '3',
  ),
  'searchable' => 
  array (
  ),
);
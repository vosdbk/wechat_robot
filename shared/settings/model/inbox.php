<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['models']['inbox']=array (
  'id' => '1',
  'name' => 'inbox',
  'description' => '收信箱',
  'perpage' => '20',
  'hasattach' => '0',
  'built_in' => '0',
  'fields' => 
  array (
    1 => 
    array (
      'id' => '1',
      'name' => 'from_user',
      'description' => '发信人',
      'model' => '1',
      'type' => 'input',
      'length' => '50',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '1',
      'editable' => '1',
    ),
    2 => 
    array (
      'id' => '2',
      'name' => 'json_data',
      'description' => '信息数据',
      'model' => '1',
      'type' => 'textarea',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '200',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '2',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '1',
  ),
  'searchable' => 
  array (
  ),
);
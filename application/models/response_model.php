<?php

class Response_model extends CI_Model {

    private $tableName = 'u_m_response';

    public function __construct() {
        parent::__construct();
    }

    public function getResponse($for_keyword_id) {
        $where['for_keyword_id'] = $for_keyword_id;

        $result = $this->db->get_where($this->tableName, $where);

        if ($result->num_rows() > 0) {
            return $result->result();
        }

        return FALSE;
    }

}
